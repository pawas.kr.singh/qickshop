from django.shortcuts import render
from django.views import View

# Create your views here.

class Bookings(View):
    template_name = 'booking/booking.html'

    def get(self, request):
        return render(request, self.template_name)