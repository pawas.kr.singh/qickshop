from django.db import models
from Apps.Auth.models import SoftDeleteModel
from django.contrib.auth import get_user_model
from Apps.Master.models import *
User = get_user_model()


class Appointments(SoftDeleteModel):
    salon = models.ForeignKey(
        SalonMaster, on_delete=models.PROTECT, null=True, blank=True)
    appoint_type = (('service', 'Service'),
                    ('package', 'Package'),
                    ('both', 'Both'))
    appoint_status = (('pending', 'Pending'),
                      ('confirmed', 'Confirmed'), ('cancelled', 'cancelled'))
    service_id = models.ManyToManyField(ServicesMaster)
    package_id = models.ManyToManyField(PackageMaster)
    customer_id = models.ForeignKey(
        User, related_name='customer', on_delete=models.PROTECT, null=True, blank=True)
    beautician = models.ForeignKey(
        BeauticianProfile, on_delete=models.PROTECT, null=True, blank=True)
    total_amount = models.CharField(max_length=100, null=True, blank=True)
    decline_reason = models.CharField(max_length=100, null=True, blank=True)
    cacelled_at = models.DateField(auto_now_add=False, null=True, blank=True)
    cancelled_by = models.ForeignKey(
        User, related_name='cancelled', on_delete=models.PROTECT, null=True, blank=True)
    apointment_date = models.DateField(
        auto_now_add=False, null=True, blank=True)
    apointment_transfer_reason = models.CharField(
        max_length=100, null=True, blank=True)
    appointment_type = models.CharField(
        choices=appoint_type, max_length=10, null=True, blank=True)
    appointment_status = models.CharField(
        choices=appoint_status, max_length=10, null=True, blank=True)
    is_rated=models.BooleanField(default=False)
    services_qty = models.TextField(null=True, blank=True)
    packages_qty = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = "Appointment"
        db_table = "Appointment"

    def __int__(self):
        return self.service_id


class CustomerOtherBooking(SoftDeleteModel):
    salon = models.ForeignKey(
        SalonMaster, on_delete=models.PROTECT, null=True, blank=True)
    customer_id = models.ForeignKey(
        User, related_name='customer_id', on_delete=models.PROTECT, null=True, blank=True)
    name=models.CharField(max_length=100, null=True, blank=True)
    mobile_no=models.CharField(max_length=10, null=True, blank=True)
    address=models.TextField(null=True, blank=True)
    appointment = models.ForeignKey(
        Appointments, on_delete=models.PROTECT, null=True, blank=True)
    class Meta:
        verbose_name = "Customer Other Booking"
        db_table = "CustomerOtherBooking"

    def __str__(self):
        return self.name


class Bookings(SoftDeleteModel):
    status = (
        ('pending', 'Pending'),
        ('completed', 'Completed')
    )
    salon = models.ForeignKey(
        SalonMaster, on_delete=models.PROTECT, null=True, blank=True)
    appointment = models.ForeignKey(
        Appointments, on_delete=models.PROTECT, null=True, blank=True)
    booking_status = models.CharField(
        choices=status, max_length=10, null=True, blank=True)
    total_amount = models.CharField(max_length=100, null=True, blank=True)
    reached_at_location_on = models.DateField(auto_now_add=False,null=True, blank=True)
    total_time_spent = models.CharField(max_length=100, null=True, blank=True)
    location = models.CharField(max_length=200, null=True, blank=True)
    latitude = models.CharField(max_length=200, null=True, blank=True)
    longitude = models.CharField(max_length=200, null=True, blank=True)
    service_started_on = models.TimeField(auto_now_add=False, null=True, blank=True)
    service_completed_on = models.TimeField(auto_now_add=False, null=True, blank=True)
    fraud_analysis_score = models.CharField(max_length=100, null=True, blank=True)
    from_time = models.TimeField(null=True, blank=True)
    to_time = models.TimeField(null=True, blank=True)
    invoice_number = models.CharField(max_length=100, null=True, blank=True)
    booking_number = models.CharField(max_length=100, null=True, blank=True)
    customer_rating = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        verbose_name = "Bookings"
        db_table = "Bookings"

    def __int__(self):
        return self.Bookings


class BookingReviews(SoftDeleteModel):
    salon = models.ForeignKey(
        SalonMaster, on_delete=models.PROTECT, null=True, blank=True)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True)
    review_deacription = models.CharField(
        max_length=100, null=True, blank=True)
    booking = models.ForeignKey(
        Bookings, on_delete=models.PROTECT, null=True, blank=True)
    rating = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        verbose_name = "Booking Reviews"
        db_table = "BookingReviews"

    def __str__(self):
        return self.rating

class BookingSos(SoftDeleteModel):
    salon = models.ForeignKey(
        SalonMaster, on_delete=models.PROTECT, null=True, blank=True)
    booking = models.ForeignKey(Bookings, on_delete=models.PROTECT, null=True, blank=True)
    sos = models.BooleanField(default=True)
    date_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Booking Sos"
        db_table = "BookingSos"

    def __int__(self):
        return self.booking
    
    
class CurrentLocation(SoftDeleteModel):
    salon = models.ForeignKey(
        SalonMaster, on_delete=models.PROTECT, null=True, blank=True)
    booking_id = models.ForeignKey(Bookings, on_delete=models.PROTECT, null=True, blank=True)
    beautician_id = models.ForeignKey(BeauticianProfile, on_delete=models.PROTECT, null=True, blank=True)
    latitude = models.CharField(max_length=200, null=True, blank=True)
    longitude = models.CharField(max_length=200, null=True, blank=True)
    routes = models.BooleanField(max_length=200, null=True, blank=True)
    
    class Meta:
        verbose_name = "Current Location"
        db_table = "CurrentLocation"

    def __int__(self):
        return self.beautician_id
