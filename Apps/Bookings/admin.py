from django.contrib import admin
from .models import *
# Register your models here.


class AppointmentsAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Appointments._meta.fields]

class CustomerOtherBookingAdmin(admin.ModelAdmin):
    list_display = [field.name for field in CustomerOtherBooking._meta.fields]

class BookingsAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Bookings._meta.fields]

class BookingReviewsAdmin(admin.ModelAdmin):
    list_display = [field.name for field in BookingReviews._meta.fields]

class BookingSosAdmin(admin.ModelAdmin):
    list_display = [field.name for field in BookingSos._meta.fields]

class CurrentLocationAdmin(admin.ModelAdmin):
    list_display = [field.name for field in CurrentLocation._meta.fields]


admin.site.register(Appointments, AppointmentsAdmin)
admin.site.register(CustomerOtherBooking, CustomerOtherBookingAdmin)
admin.site.register(Bookings, BookingsAdmin)
admin.site.register(BookingReviews, BookingReviewsAdmin)
admin.site.register(BookingSos, BookingSosAdmin)
admin.site.register(CurrentLocation, CurrentLocationAdmin)