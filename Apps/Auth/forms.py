from django import forms
from .models import *
from django.forms import ModelForm
from django.contrib.auth.hashers import check_password


class SignInForm(forms.Form):
    email_username = forms.CharField(max_length=30, required=False)
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)