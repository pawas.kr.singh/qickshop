from datetime import timezone
from django.contrib.auth.models import AbstractUser
from django.db import models
from .manager import SoftDeletionManager


class User(AbstractUser):
    GENDER = (('male', 'Male'), ('female', 'Female'), ('other', 'Other'))
    username = models.CharField(max_length=150, unique=True)
    email_id = models.EmailField(unique=True, max_length=250)
    mobile = models.CharField(max_length=100, null=True, blank=True)
    first_name = models.CharField(max_length=150, null=True)
    last_name = models.CharField(max_length=150, null=True)
    age = models.IntegerField(default=0)
    gender = models.CharField(max_length=100, choices=GENDER, null=True, default=None, blank=True)
    avatar = models.ImageField(upload_to='avatar/', null=True, blank=True)
    referral_code = models.CharField(max_length=500, null=True, blank=True)
    how_did_you_hear = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.email

    def __int__(self):
        return self.id

    class Meta:
        verbose_name_plural = 'Users'
        db_table = 'User'


class SoftDeleteModel(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField(null=True, default=1)
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.IntegerField(null=True, blank=True, default=None)
    is_active = models.BooleanField(default=True)
    deleted_by = models.IntegerField(null=True, blank=True, default=None)
    deleted_on = models.DateTimeField(null=True, default=None, blank=True)

    objects = SoftDeletionManager()
    all_objects = SoftDeletionManager(alive_only=False)

    class Meta:
        abstract = True

    def delete(self):
        self.deleted_on = timezone.now()
        self.save()