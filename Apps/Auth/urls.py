from django.urls import path
from . import views

#url pattern

urlpatterns = [
    path('login/', views.Login.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name="logout"),
    path('user-login-details', views.Registration.as_view(), name='registration'),
    path('registration-your-salon', views.RegistraterSaloon.as_view(), name='salon_registration'),
    
]