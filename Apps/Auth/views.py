from django import views
from django.shortcuts import render, redirect
from django.views import View
from .forms import *
from django.contrib.auth import authenticate, login, logout

# Create your views here.


class Login(View):
    template_name = 'log_reg/login.html'

    form_class = SignInForm
    # template_name = 'login-register/login.html'

    def get(self, request):
        form = self.form_class(request.POST)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if request.POST['email_username']:
            user = authenticate(email_id=request.POST['email_username'], password=request.POST['password'])
            if user is None:
                user = authenticate(username=request.POST['email_username'], password=request.POST['password'])
        if user:
            login(request, user)
            return redirect('dashboard')
        else:
            context = {
                'error': "Either username or password incorrect"
            }
            return render(request, self.template_name, {'form': form, 'context': context})


class Registration(View):
    template_name = 'log_reg/user_log.html'

    def get(self, request):
        return render(request, self.template_name)


    def post(self, request):
        return redirect('salon_registration')


class RegistraterSaloon(View):
    template_name = 'log_reg/salon_regis.html'

    def get(self, request):
        return render(request, self.template_name)


    def post(self, request):
        return render(request, self.template_name)


class LogoutView(View):
    
    def get(self, request):
        logout(request)
        return redirect('login')