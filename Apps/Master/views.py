from http import server
from webbrowser import get
from django import views
from django.shortcuts import redirect, render
from django.views import View
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string
from .forms import *
from .models import *
from django.views.generic import (CreateView,  TemplateView,
                                  UpdateView)
from .services import master_services

# Create your views here.

def send_email(to, ctx):
        subject = "Request From Consumer"
        message = render_to_string("email/contact_us_email.html", ctx)
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, to, html_message=message)

class Home(View):
    template_name = 'master/home/index.html'

    def get(self, request):
        return render(request, self.template_name)
    
    def post(self, request):
        ctx={
            'name':request.POST.get('name'),
            'email':request.POST.get('email'),
            'mobile':request.POST.get('mobile'),
            'message':request.POST.get('message')
        }
        send_email((settings.DEFAULT_FROM_EMAIL,), ctx)
        return render(request, self.template_name)



class Dashboard(View):
    template_name = 'admin/dashboard.html'

    def get(self, request):
        return render(request, self.template_name)
    
    # def post(self, request):
    #     ctx={
    #         'name':request.POST.get('name'),
    #         'email':request.POST.get('email'),
    #         'mobile':request.POST.get('mobile'),
    #         'message':request.POST.get('message')
    #     }
    #     send_email((settings.DEFAULT_FROM_EMAIL,), ctx)
    #     return render(request, self.template_name)


class Servives(View):
    template_name = 'master/service/service.html'

    def get(self, request):
        return render(request, self.template_name)


class CreateServives(View):
    template_name = 'master/service/create_service.html'

    def get(self, request):
        return render(request, self.template_name)


class Packages(View):
    template_name = 'master/package/packages.html'
    form_class = PackageForm

    def get(self, request):
        return render(request, self.template_name)


class PackageMasterCreate(CreateView):
    form_class = PackageForm
    template_name = "master/package/package_create.html"

    def form_valid(self, form):
        make = form.save(commit=False)
        make.aproximate_time = self.request.POST.get('aprox_time')
        make.save()
        Servicellist = self.request.POST.getlist('services')
        price = ServicesMaster.objects.filter(pk__in=Servicellist, is_active=True, is_deleted=False)
        make.services.set(price)
        return redirect('packages')


class Customers(View):
    template_name = 'master/customer.html'

    def get(self, request):
        return render(request, self.template_name)



class Beautician(View):
    template_name = 'master/beautician/beautician.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        return render(request, self.template_name)


class CreateBeautician(View):
    template_name = 'master/beautician/create_beautician.html'

    def get(self, request):
        return render(request, self.template_name)


class Review(View):
    template_name = 'master/review.html'

    def get(self, request):
        return render(request, self.template_name)


class Category(View):
    template_name = 'master/category.html'

    def get(self, request):
        return render(request, self.template_name)


class WebServives(View):
    template_name = 'master/service/web_services.html'

    def get(self, request, service_type):
        services = ''
        if service_type =='male-service':
            services = master_services['male']
        else:
            services = master_services[service_type]
        return render(request, self.template_name, context={"types":service_type,'services':services})