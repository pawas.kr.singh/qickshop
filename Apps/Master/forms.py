from django import forms
from . import models
from Apps.Master.models import *


class SalonForm(forms.ModelForm):
    class Meta:
        model = SalonMaster
        fields = ('own_by', 'salon_name', 'description', 'salon_logo', 'address_line1','address_line2', 'address_city', 'address_state')


class BeauticianProfileForm(forms.ModelForm):
    class Meta:
        model = BeauticianProfile
        fields = ('salon', 'beautician', 'aadhar_card', 'pan_card', 'is_verified','verified_by', 
                'last_verified_on', 'verification_valid_till', 'total_bookings', 'average_ratings', 'available')


class ServicesForm(forms.ModelForm):
    class Meta:
        model = ServicesMaster
        fields = ('salon', 'name', 'image', 'discount','category','price','description','aproximate_time')


class PackageForm(forms.ModelForm):
    package_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'Package Name',"class":"form-control"}))
    price = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'Price',"class":"form-control"}))
    description = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'rows': 4, 'cols': 40, 'placeholder': 'Description',"class":"form-control"}))
    image = forms.ImageField(required=True)
    discount = forms.CharField(required=True, label="Discount <small style='font-size: 10px'> %</small>",
                               widget=forms.TextInput(attrs={'placeholder': 'Ex: 50',"class":"form-control"}))
    services = forms.ModelMultipleChoiceField(label="All Services", queryset=ServicesMaster.objects.filter(is_active=True
                                                                                                           ), widget=forms.CheckboxSelectMultiple(attrs={'class': 'services form-control'}))
    class Meta:
        model = PackageMaster
        fields = ('salon', 'package_name', 'image', 'services','price','discount','description','aproximate_time','beautician')