from django.db import models
from Apps.Auth.models import SoftDeleteModel
from django.contrib.auth import get_user_model

# from django.utils.translation import ugettext as _
User = get_user_model()

# ----------------------models starts------------------------


class SalonMaster(SoftDeleteModel):
    salon_name = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    own_by = models.ForeignKey(User, on_delete=models.SET_NULL,
                               null=True, blank=True,
                             related_name='own_by')
    salon_logo = models.ImageField(upload_to='SalonProfile/', null=True, blank=True)
    address_line1 = models.CharField(max_length=250, null=True)
    address_line2 = models.CharField(max_length=250, null=True)
    address_city = models.CharField(max_length=200, null=True)
    address_state = models.CharField(max_length=200, null=True)

    class Meta:
        verbose_name = "Salon Master"
        db_table = "SalonMaster"

    def __str__(self):
        return self.salon_name


class BeauticianProfile(SoftDeleteModel):
    salon = models.ForeignKey(
        SalonMaster, on_delete=models.PROTECT, null=True, blank=True)
    beautician = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True)
    aadhar_card = models.ImageField(
        upload_to="aadhar_card/", null=True, blank=True)
    pan_card = models.ImageField(upload_to="pan_card/", null=True, blank=True)
    is_verified = models.BooleanField(default=False)
    verified_by = models.ForeignKey(
        User, related_name='user_verified', on_delete=models.PROTECT, null=True, blank=True)
    last_verified_on = models.DateField(
        auto_now_add=False, null=True, blank=True)
    verification_valid_till = models.DateField(
        auto_now_add=False, null=True, blank=True)
    total_bookings = models.CharField(max_length=100, null=True, blank=True)
    average_ratings = models.CharField(max_length=100, null=True, blank=True)
    available = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Beautician Profile"
        db_table = "BeauticianProfile"

    def __int__(self):
        return self.beautician


class CategoryMaster(SoftDeleteModel):
    salon = models.ForeignKey(
        SalonMaster, on_delete=models.PROTECT, null=True, blank=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    parent_category = models.IntegerField(null=True, blank=True)
    description = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        verbose_name = "Category Master"
        db_table = "CategoryMaster"

    def __str__(self):
        return self.name


class ServicesMaster(SoftDeleteModel):
    salon = models.ForeignKey(
        SalonMaster, on_delete=models.PROTECT, null=True, blank=True)
    image = models.ImageField(
        upload_to="service_image/", null=True, blank=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    discount = models.CharField(max_length=100, null=True, blank=True)
    category = models.ForeignKey(
        CategoryMaster, on_delete=models.PROTECT, null=True, blank=True)
    price = models.CharField(max_length=100, null=True, blank=True)
    description = models.CharField(max_length=300, null=True, blank=True)
    aproximate_time = models.CharField(
        max_length=80, null=True, blank=True, default=0)

    class Meta:
        verbose_name = "Services Master"
        db_table = "ServicesMaster"

    def __str__(self):
        return self.name


class PackageMaster(SoftDeleteModel):
    salon = models.ForeignKey(
        SalonMaster, on_delete=models.PROTECT, null=True, blank=True)
    image = models.ImageField(
        upload_to="package_image/", null=True, blank=True)
    services = models.ManyToManyField(ServicesMaster)
    package_name = models.CharField(max_length=100, null=True, blank=True)
    price = models.CharField(max_length=100, null=True, blank=True)
    discount = models.CharField(max_length=100, null=True, blank=True)
    beautician = models.ForeignKey(
        BeauticianProfile, on_delete=models.PROTECT, null=True, blank=True)
    description = models.CharField(max_length=300, null=True, blank=True)
    aproximate_time = models.CharField(
        max_length=80, null=True, blank=True, default=0)

    class Meta:
        verbose_name = "Package Master"
        db_table = "PackageMaster"

    def __str__(self):
        return self.package_name

class MobileOtp(SoftDeleteModel):    
    salon = models.ForeignKey(
        SalonMaster, on_delete=models.PROTECT, null=True, blank=True)
    mobile_no = models.CharField(max_length=10, null=True, blank=True)
    otp = models.CharField(max_length=100, null=True, blank=True)
    is_verified = models.BooleanField(default=False)
    beautician_verify = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Mobile Otp"
        db_table = "mobile_otp"
