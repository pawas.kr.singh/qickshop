master_services={
    'male':{
            'title':'Male(Cut, Colour, Style & Treatment)',
            'items':
                [
                    {'name':'',
                    'all_services':['Hair Cut with Wash','Hairwash Conditioner','Child Hair Cut','Scrub','Black Heads','Trim','Keratin',
                    'Straightening (Loreal)','Straightening (Schwarzkopf)', 'Shaving','Cleusing','Beard','Stylish Haircut',
                    'Hair Spa (Loreal)','Hair Spa (Schwarzkopf)','Hair Styling','Head Massage','Hair Color Application','Beard Color',
                    'Ammonia Free Color Application','Hair Spa Treatment','Groom Makeup']
                    }
                ]
            },
    'professional-hair-care':{
        'title':'Professional Hair Care, Styling, Coloring, Hair Spa & Rebonding',
        'items':
                [
                    {
                        'name':'Hair Essentials',
                        'all_services':['Haircut with Wash','Hair Wash + Conditioner','Child Hair Cut','Straightening (Loreal)','Straightening (Schwarzkopf)',
                            'Smoothening (Loreal)','Smoothening (Schwarzkopf)', 'Kera Bounding','Hair Botox','Blow Dry','Hair Spa (Loreal)',
                            'Hair Spa (Normal)']
                    },
                    {
                        'name':'Hair Colouring',
                        'all_services':['Hair Trimming','Head Massage','Color Application(Root Touchup)','Color Application (Ammonia Free)','Global Color',
                            'Highlights (Per Stick)','Henna Application', 'Trimming/Crimping/Tong','Hair Spa Treatment']
                    },
                    {
                        'name':'Hair Spa & Rebonding',
                        'all_services':['Loreal Hair Spa','Smoothening','Keratin','Rebonding']
                    }
                ]
    },
    'bleaching-clean-up-facial':{
        'title':'Bleaching, Clean-up & Facial',
        'items':
                [
                    {
                        'name':'Bleaching',
                        'all_services':['Fruit','Oxy','Gold','Diamond','Detan', 'Full Arm','Under Arm', 'Full Body','Back']
                    },
                    {
                        'name':'Cleanup',
                        'all_services':['Natures','VLCC','O3 +','Aroma']
                    },
                    {
                        'name':'Facial',
                        'all_services':['<h3>LOTUS</h3><br>','Normal','Luxary','<h3>Natures</h3><br>','Gold Facial',
                        'Diamond Facial','Fruit Facial','<h3>VLCC</h3><br>', 'Normal','Luxary','<h3>Aroma</h3><br>',
                        'Fruit','Whitening','Bridal Facial','<h3>O3+</h3><br>','Pure Brightening','Express Whitening','Skin Tightening',
                        'Adult Acne','Anti Aging']
                    }
                ]
    },
    'manicure-pedicure':{
        'title':'Bleaching, Clean-up & Facial',
        'items':
                [
                    {
                        'name':'',
                        'all_services':['Nail Cut & Filing','Nail Polish with Cut','Hand Massage','Foot Massage','Nail Extension', 'Aroma Manicure','Aroma Pedicure', 'Lotus Manicure','Lotus Pedicure']
                    }
                ]
    },
    'grooming-waxing':{
        'title':'Bleaching, Clean-up & Facial',
        'items':
                [
                    {
                        'name':'Waxing',
                        'all_services':['Upper Lips','Fore Head','Chin','Side Lock','Neck', 'Nose','Full Face']
                    },
                    {
                        'name':'Waxing (Rica)',
                        'all_services':['Full Leg','Half Leg','Half Arm','Full Arm','Under Arms','Back','Tummy','Front','Full Body',
                            'Chest','Body Polishing(with Oil)','Body Polishing(with Spa)']
                    },
                    {
                        'name':'Grooming',
                        'all_services':['Eye Brow','Upper Lips','Forehead','Chin','Side Lock',
                        'Neck','Black Heads','Eye Makeup','<br><p>Note : Male Wax Service Extra by Staff for per Service.</p>']
                    }
                ]
    },
    'getup-makeup':{
        'title':'Getup & Makeup',
        'items':
                [
                    {
                        'name':'Party Makeup',
                        'all_services':['Kraylon','MAC','Forever 52']
                    },
                    {
                        'name':'Reception Makeup',
                        'all_services':['Kraylon','MAC','Forever 52','Air Brush']
                    },
                    {
                        'name':'Engagement Makeup',
                        'all_services':['Kraylon','MAC','Forever 52','Air Brush']
                    },
                    {
                        'name':'Bridal Makeup',
                        'all_services':['Kraylon','MAC','Forever 52','Air Brush' ,'Boby Brown','Boby Brown Air Brush']
                    },
                    {
                        'name':'Hair Do',
                        'all_services':['Normal Hair do','Stylish Hair do','Sari Draping','Student Makeup']
                    },
                    {
                        'name':'Fantasy Makeup',
                        'all_services':[]
                    },
                    {
                        'name':'Eye Makeup',
                        'all_services':[]
                    }
                ]
    }
}
