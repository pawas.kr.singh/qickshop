from django.contrib import admin
from .models import *
# Register your models here.


class SalonMasterAdmin(admin.ModelAdmin):
    search_fields = ['salon_name', 'address_city']
    list_display = [field.name for field in SalonMaster._meta.fields]

class BeauticianProfileAdmin(admin.ModelAdmin):
    # search_fields = ['salon_name', 'address_city']
    list_display = [field.name for field in BeauticianProfile._meta.fields]

class CategoryMasterAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = [field.name for field in CategoryMaster._meta.fields]

class ServicesMasterAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = [field.name for field in ServicesMaster._meta.fields]

class PackageMasterAdmin(admin.ModelAdmin):
    search_fields = ['package_name']
    list_display = [field.name for field in PackageMaster._meta.fields]

class MobileOtpAdmin(admin.ModelAdmin):
    search_fields = ['mobile_no']
    list_display = [field.name for field in MobileOtp._meta.fields]


admin.site.register(SalonMaster, SalonMasterAdmin)
admin.site.register(BeauticianProfile, BeauticianProfileAdmin)
admin.site.register(CategoryMaster, CategoryMasterAdmin)
admin.site.register(ServicesMaster, ServicesMasterAdmin)
admin.site.register(PackageMaster, PackageMasterAdmin)
admin.site.register(MobileOtp, MobileOtpAdmin)