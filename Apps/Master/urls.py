from django.urls import path
from . import views
from django.contrib.auth.decorators import login_required

#url pattern

urlpatterns = [
    path('', views.Home.as_view(), name='home'),
    path('dashboard', login_required(views.Dashboard.as_view()), name='dashboard'),
    path('servives', login_required(views.Servives.as_view()), name='servives'),
    path('create-servives', login_required(views.CreateServives.as_view()), name='create_servives'),
    path('packages-list', login_required(views.Packages.as_view()), name='packages'),
    path('packages-create', login_required(views.PackageMasterCreate.as_view()), name='create_packages'),
    path('customers', login_required(views.Customers.as_view()), name='customers'),
    path('beautician', login_required(views.Beautician.as_view()), name='beautician'),
    path('create-beautician', login_required(views.CreateBeautician.as_view()), name='create_beautician'),
    path('review', login_required(views.Review.as_view()), name='review'),
    path('category', login_required(views.Category.as_view()), name='category'),
    path('servives/<str:service_type>', views.WebServives.as_view(), name='web_servives'),
]