# Generated by Django 4.0.3 on 2022-06-05 12:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('Master', '0003_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SalonMaster',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(default=1, null=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(blank=True, default=None, null=True)),
                ('is_active', models.BooleanField(default=True)),
                ('deleted_by', models.IntegerField(blank=True, default=None, null=True)),
                ('deleted_on', models.DateTimeField(blank=True, default=None, null=True)),
                ('salon_name', models.CharField(blank=True, max_length=100, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('salon_logo', models.ImageField(blank=True, null=True, upload_to='SalonProfile/')),
                ('address_line1', models.CharField(max_length=250, null=True)),
                ('address_line2', models.CharField(max_length=250, null=True)),
                ('address_city', models.CharField(max_length=200, null=True)),
                ('address_state', models.CharField(max_length=200, null=True)),
                ('own_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='own_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Salon Master',
                'db_table': 'SalonMaster',
            },
        ),
    ]
