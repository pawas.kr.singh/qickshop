from django.contrib import admin
from .models import *
# Register your models here.


class PaymentsAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Payments._meta.fields]

class CollectionMasterAdmin(admin.ModelAdmin):
    list_display = [field.name for field in CollectionMaster._meta.fields]

class PaymentTransferAdmin(admin.ModelAdmin):
    list_display = [field.name for field in PaymentTransfer._meta.fields]

# class ServicesMasterAdmin(admin.ModelAdmin):
#     list_display = [field.name for field in ServicesMaster._meta.fields]

# class PackageMasterAdmin(admin.ModelAdmin):
#     list_display = [field.name for field in PackageMaster._meta.fields]

# class MobileOtpAdmin(admin.ModelAdmin):
#     list_display = [field.name for field in MobileOtp._meta.fields]


admin.site.register(Payments, PaymentsAdmin)
admin.site.register(CollectionMaster, CollectionMasterAdmin)
admin.site.register(PaymentTransfer, PaymentTransferAdmin)
# admin.site.register(ServicesMaster, ServicesMasterAdmin)
# admin.site.register(PackageMaster, PackageMasterAdmin)
# admin.site.register(MobileOtp, MobileOtpAdmin)