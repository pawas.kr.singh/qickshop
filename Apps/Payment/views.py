from django.shortcuts import render
from django.views import View

# Create your views here.

class Payment(View):
    template_name = 'payment/payment.html'

    def get(self, request):
        return render(request, self.template_name)