from django.db import models
from Apps.Auth.models import SoftDeleteModel
from django.contrib.auth import get_user_model
from Apps.Bookings.models import *
from Apps.Master.models import *
User = get_user_model()


class Payments(SoftDeleteModel):
    method = (('card', 'Card'),
              ('cash', 'Cash'),
              ('paytm', 'Paytm'))

    taken_by = (
        ('system', 'System'),
        ('beautician', 'Beautician'))

    payment_method = models.CharField(
        choices=method, max_length=10, null=True, blank=True)
    booking = models.ForeignKey(
        BeauticianProfile, on_delete=models.PROTECT, null=True, blank=True)
    amount_to_be_paid = models.CharField(max_length=100, null=True, blank=True)
    amount_paid = models.CharField(max_length=100, null=True, blank=True)
    payment_date = models.DateTimeField(auto_now_add=False)
    service_started_on = models.DateTimeField(auto_now_add=False, null=True, blank=True)
    transaction_number = models.CharField(
        max_length=100, null=True, blank=True)
    booking_number = models.CharField(max_length=100, null=True, blank=True)
    invoice_number = models.CharField(max_length=100, null=True, blank=True)
    payment_taken_by = models.CharField(
        choices=taken_by, max_length=10, null=True, blank=True)
    is_settlement = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Payments"
        db_table = "Payments"

    def __int__(self):
        return self.Payments


class CollectionMaster(SoftDeleteModel):
    booking = models.ForeignKey(
        BeauticianProfile, on_delete=models.PROTECT, null=True, blank=True)
    total_amount = models.CharField(max_length=100, null=True, blank=True)
    amount_collected = models.CharField(max_length=100, null=True, blank=True)
    amount_due = models.CharField(max_length=100, null=True, blank=True)
    collected_on = models.DateTimeField(auto_now_add=False)
    collected_by = models.DateTimeField(auto_now_add=False)
    comments = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        verbose_name = "Collection Master"
        db_table = "CollectionMaster"

    def __str__(self):
        return self.total_amount


class PaymentTransfer(SoftDeleteModel):
    booking = models.ForeignKey(
        BeauticianProfile, on_delete=models.PROTECT, null=True, blank=True)
    total_amount = models.CharField(max_length=100, null=True, blank=True)
    amount_paid = models.CharField(max_length=100, null=True, blank=True)
    amount_due = models.CharField(max_length=100, null=True, blank=True)
    transfered_on = models.DateTimeField(auto_now_add=False)
    transfered_by = models.DateTimeField(auto_now_add=False)
    comments = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        verbose_name = "Payment Transfer"
        db_table = "PaymentTransfer"

    def __str__(self):
        return self.total_amount
